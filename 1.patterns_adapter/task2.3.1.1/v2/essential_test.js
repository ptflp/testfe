import Adapter from './Adapter';

describe('Adapter', () => {
  let adaptee;
  let adapter;

  beforeEach(() => {
    adaptee = {
      method1: jest.fn(),
      method2: jest.fn(),
    };
    adapter = new Adapter(adaptee);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should call adaptee.method1 when method1 is called', () => {
    adapter.method1();
    expect(adaptee.method1).toHaveBeenCalled();
  });

  it('should call adaptee.method2 when method2 is called', () => {
    adapter.method2();
    expect(adaptee.method2).toHaveBeenCalled();
  });
});
