import React from 'react';
import ThirdPartyLibrary from 'third-party-library';

class ThirdPartyLibraryAdapter {
  // Реализация адаптера
  someMethod() {
    // вызов метода сторонней библиотеки
    ThirdPartyLibrary.someMethod();
  }
}

class MyComponent extends React.Component {
  constructor(props) {
    super(props);
    this.thirdPartyLibrary = new ThirdPartyLibraryAdapter();
  }

  // Дополнительный код компонента

  render() {
    return (
      <div>
        {/* Использование адаптера */}
        <button onClick={() => this.thirdPartyLibrary.someMethod()}>Call Third Party Library Method</button>
      </div>
    );
  }
}
