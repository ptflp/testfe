import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import ThirdPartyLibrary from 'third-party-library';
import MyComponent from './MyComponent';

jest.mock('third-party-library');

describe('MyComponent', () => {
  test('should call the Third Party Library method when the button is clicked', () => {
    // Render the component
    const { getByText } = render(<MyComponent />);

    // Simulate a button click
    fireEvent.click(getByText('Call Third Party Library Method'));

    // Verify that the Third Party Library method was called
    expect(ThirdPartyLibrary.someMethod).toHaveBeenCalled();
  });
});
