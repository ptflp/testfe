import { render, screen } from '@testing-library/react';
import Adapter from './Adapter';
import Adaptee from './Adaptee';

describe('Adapter', () => {
  test('should call specificRequest method when request is called', () => {
    // Arrange
    const adaptee = new Adaptee();
    const adapter = new Adapter(adaptee);
    const specificRequestSpy = jest.spyOn(adaptee, 'specificRequest');

    // Act
    adapter.request();

    // Assert
    expect(specificRequestSpy).toHaveBeenCalled();
  });
});
