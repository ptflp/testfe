// Интерфейс, ожидаемый объектом, с которым нужно работать
class TargetInterface {
  request() {
    throw new Error('Метод request должен быть реализован');
  }
}

// Объект, с которым нужно работать
class Adaptee {
  specificRequest() {
    console.log('Вызван метод specificRequest');
  }
}

// Адаптер
class Adapter extends TargetInterface {
  constructor(adaptee) {
    super();
    this.adaptee = adaptee;
  }

  request() {
    this.adaptee.specificRequest();
  }
}

// Использование адаптера
const adaptee = new Adaptee();
const adapter = new Adapter(adaptee);
adapter.request();
