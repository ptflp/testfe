# Проверяемое задание: Паттерн адаптер в React JS

# Описание:
Ваша задача - реализовать паттерн адаптер в React JS. Паттерн адаптер позволяет объектам с несовместимыми интерфейсами работать вместе. Вам необходимо создать адаптер, который будет преобразовывать интерфейс одного объекта в интерфейс, ожидаемый другим объектом.

```
// Пример кода, который может быть предоставлен:

// Интерфейс, ожидаемый объектом, с которым нужно работать
class TargetInterface {
  request() {
    throw new Error('Метод request должен быть реализован');
  }
}

// Объект, с которым нужно работать
class Adaptee {
  specificRequest() {
    console.log('Вызван метод specificRequest');
  }
}

// Адаптер
class Adapter extends TargetInterface {
  constructor(adaptee) {
    super();
    this.adaptee = adaptee;
  }

  request() {
    this.adaptee.specificRequest();
  }
}

// Использование адаптера
const adaptee = new Adaptee();
const adapter = new Adapter(adaptee);
adapter.request();
```

# Критерии приемки:

- Создан адаптер, который преобразует интерфейс объекта Adaptee в интерфейс TargetInterface.
- Метод request в адаптере вызывает метод specificRequest у объекта Adaptee.
- При запуске программы выводится сообщение "Вызван метод specificRequest".

Решение расположи по следующему пути: course2/3.patterns/1.patterns_adapter/task2.3.1.1