# Вопросы: Паттерны. Глава: Паттерн адаптер.

## Что такое паттерн адаптер?
- [ ] Структурный паттерн проектирования, который позволяет объектам с несовместимыми интерфейсами работать вместе.
- [ ] Объект с несовместимым интерфейсом, который требуется адаптировать для работы с другими объектами.
- [ ] Интерфейс, который ожидается от адаптированного объекта и который должен быть совместим с другими объектами.
- [ ] Класс, который реализует целевой интерфейс и обеспечивает взаимодействие между адаптированным объектом и другими объектами.

## Какой компонент паттерна адаптер преобразует вызовы методов целевого интерфейса в вызовы методов адаптированного объекта?
- [ ] Целевой интерфейс
- [ ] Адаптированный объект
- [ ] Адаптер
- [ ] Посредник

## Какие проблемы решает паттерн адаптер?
- [ ] Несовместимость интерфейсов объектов
- [ ] Необходимость изменения исходного кода объектов
- [ ] Отсутствие возможности работы объектов вместе
- [ ] Необходимость создания новых объектов

## Какие компоненты включает в себя структура паттерна адаптер?
- [ ] Целевой интерфейс, адаптированный объект, посредник
- [ ] Целевой интерфейс, адаптированный объект, адаптер
- [ ] Целевой интерфейс, адаптер, посредник
- [ ] Целевой интерфейс, адаптер, адаптированный объект

## Какие преимущества имеет применение паттерна адаптер?
- [ ] Упрощение интеграции компонентов с разными интерфейсами
- [ ] Возможность повторного использования существующего кода с несовместимым интерфейсом
- [ ] Обеспечение совместимости между объектами с разными интерфейсами
- [ ] Все вышеперечисленное

## Как паттерн адаптер может быть использован в React JS?
- [ ] Для создания новых компонентов с разными интерфейсами
- [ ] Для изменения интерфейса существующих компонентов
- [ ] Для обеспечения совместимости между компонентами с разными интерфейсами
- [ ] Для упрощения интеграции компонентов с разными интерфейсами

## Какой из вариантов является правильным определением паттерна адаптер?
- [ ] Паттерн адаптер - это структурный паттерн проектирования, который позволяет объектам с несовместимыми интерфейсами работать вместе.
- [ ] Паттерн адаптер - это объект с несовместимым интерфейсом, который требуется адаптировать для работы с другими объектами.
- [ ] Паттерн адаптер - это интерфейс, который ожидается от адаптированного объекта и который должен быть совместим с другими объектами.
- [ ] Паттерн адаптер - это класс, который реализует целевой интерфейс и обеспечивает взаимодействие между адаптированным объектом и другими объектами.

## Какие компоненты включает в себя структура паттерна адаптер?
- [ ] Целевой интерфейс, адаптированный объект, посредник
- [ ] Целевой интерфейс, адаптированный объект, адаптер
- [ ] Целевой интерфейс, адаптер, посредник
- [ ] Целевой интерфейс, адаптер, адаптированный объект

## Какие преимущества имеет применение паттерна адаптер?
- [ ] Упрощение интеграции компонентов с разными интерфейсами
- [ ] Возможность повторного использования существующего кода с несовместимым интерфейсом
- [ ] Обеспечение совместимости между объектами с разными интерфейсами
- [ ] Все вышеперечисленное

## Как паттерн адаптер может быть использован в React JS?
- [ ] Для создания новых компонентов с разными интерфейсами
- [ ] Для изменения интерфейса существующих компонентов
- [ ] Для обеспечения совместимости между компонентами с разными интерфейсами
- [ ] Для упрощения интеграции компонентов с разными интерфейсами

## Какой из вариантов является правильным определением паттерна адаптер?
- [ ] Паттерн адаптер - это структурный паттерн проектирования, который позволяет объектам с несовместимыми интерфейсами работать вместе.
- [ ] Паттерн адаптер - это объект с несовместимым интерфейсом, который требуется адаптировать для работы с другими объектами.
- [ ] Паттерн адаптер - это интерфейс, который ожидается от адаптированного объекта и который должен быть совместим с другими объектами.
- [ ] Паттерн адаптер - это класс, который реализует целевой интерфейс и обеспечивает взаимодействие между адаптированным объектом и другими объектами.

## Какой компонент паттерна адаптер преобразует вызовы методов целевого интерфейса в вызовы методов адаптированного объекта?
- [ ] Целевой интерфейс
- [ ] Адаптированный объект
- [ ] Адаптер
- [ ] Посредник

## Какие проблемы решает паттерн адаптер?
- [ ] Несовместимость интерфейсов объектов
- [ ] Необходимость изменения исходного кода объектов
- [ ] Отсутствие возможности работы объектов вместе
- [ ] Необходимость создания новых объектов

## Что такое паттерн адаптер?
- [ ] Структурный паттерн проектирования, который позволяет объектам с несовместимыми интерфейсами работать вместе.
- [ ] Объект с несовместимым интерфейсом, который требуется адаптировать для работы с другими объектами.
- [ ] Интерфейс, который ожидается от адаптированного объекта и который должен быть совместим с другими объектами.
- [ ] Класс, который реализует целевой интерфейс и обеспечивает взаимодействие между адаптированным объектом и другими объектами.