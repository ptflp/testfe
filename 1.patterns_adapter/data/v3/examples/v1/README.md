# Примеры использования

## Пример 1

В этом примере мы рассмотрим адаптерный паттерн для работы с разными типами API. У нас есть два класса, один работает с REST API, а другой с GraphQL API. Мы создадим адаптер, который будет преобразовывать вызовы методов одного интерфейса в вызовы методов другого интерфейса.

```jsx
// Класс для работы с REST API
class RestApi {
  fetchData(url) {
    // Логика получения данных через REST API
  }
}

// Класс для работы с GraphQL API
class GraphqlApi {
  fetchData(query) {
    // Логика получения данных через GraphQL API
  }
}

// Адаптер для работы с обоими типами API
class ApiAdapter {
  constructor(api) {
    this.api = api;
  }

  fetchData(urlOrQuery) {
    if (typeof urlOrQuery === 'string') {
      // Преобразование вызова метода для REST API
      return this.api.fetchData(urlOrQuery);
    } else {
      // Преобразование вызова метода для GraphQL API
      return this.api.fetchData(urlOrQuery);
    }
  }
}

// Использование адаптера
const restApi = new RestApi();
const graphqlApi = new GraphqlApi();

const apiAdapter = new ApiAdapter(restApi);
apiAdapter.fetchData('/api/data'); // Вызов метода через адаптер для REST API

apiAdapter.api = graphqlApi;
apiAdapter.fetchData('query { data }'); // Вызов метода через адаптер для GraphQL API
```

## Пример 2

В этом примере мы рассмотрим адаптерный паттерн для работы с разными типами компонентов в React JS. У нас есть компоненты, разработанные с использованием разных библиотек или фреймворков, и их интерфейсы несовместимы. Мы создадим адаптеры, которые преобразуют вызовы методов одного интерфейса в вызовы методов другого интерфейса, чтобы они могли работать вместе.

```jsx
// Компонент, разработанный с использованием библиотеки A
class ComponentA {
  render() {
    // Логика рендеринга компонента A
  }
}

// Компонент, разработанный с использованием библиотеки B
class ComponentB {
  display() {
    // Логика отображения компонента B
  }
}

// Адаптер для компонента A
class ComponentAAdapter {
  constructor(component) {
    this.component = component;
  }

  render() {
    // Преобразование вызова метода для компонента A
    return this.component.render();
  }
}

// Адаптер для компонента B
class ComponentBAdapter {
  constructor(component) {
    this.component = component;
  }

  render() {
    // Преобразование вызова метода для компонента B
    return this.component.display();
  }
}

// Использование адаптеров
const componentA = new ComponentA();
const componentB = new ComponentB();

const componentAAdapter = new ComponentAAdapter(componentA);
componentAAdapter.render(); // Вызов метода через адаптер для компонента A

const componentBAdapter = new ComponentBAdapter(componentB);
componentBAdapter.render(); // Вызов метода через адаптер для компонента B
```

В этих примерах мы продемонстрировали использование адаптерного паттерна для работы с разными типами API и компонентов в React JS. Адаптеры позволяют объектам с несовместимыми интерфейсами работать вместе, обеспечивая совместимость и возможность работы без изменения их исходного кода.