# Примеры использования

## Пример 1

В данном примере мы рассмотрим использование паттерна адаптер для адаптации двух классов с несовместимыми интерфейсами.

```jsx
// Класс, который нужно адаптировать
class LegacyComponent {
  doSomethingLegacy() {
    console.log("Выполняется старый метод");
  }
}

// Целевой интерфейс
class TargetInterface {
  doSomething() {
    console.log("Выполняется новый метод");
  }
}

// Адаптер
class Adapter extends TargetInterface {
  constructor(legacyComponent) {
    super();
    this.legacyComponent = legacyComponent;
  }

  doSomething() {
    this.legacyComponent.doSomethingLegacy();
  }
}

// Использование адаптера
const legacyComponent = new LegacyComponent();
const adapter = new Adapter(legacyComponent);

adapter.doSomething(); // Выполняется старый метод
```

В данном примере у нас есть класс `LegacyComponent`, который имеет метод `doSomethingLegacy`, и класс `TargetInterface`, который имеет метод `doSomething`. Их интерфейсы несовместимы. Мы создаем адаптер `Adapter`, который наследуется от `TargetInterface` и принимает в конструкторе экземпляр `LegacyComponent`. В методе `doSomething` адаптер вызывает метод `doSomethingLegacy` у `LegacyComponent`, преобразуя вызов метода `doSomething` в вызов метода `doSomethingLegacy`. Таким образом, адаптер позволяет объектам с несовместимыми интерфейсами работать вместе.

## Пример 2

В этом примере мы рассмотрим использование паттерна адаптер для адаптации функционального компонента React с несовместимым интерфейсом.

```jsx
// Функциональный компонент с несовместимым интерфейсом
const LegacyComponent = ({ name }) => {
  console.log(`Привет, ${name}!`);
};

// Компонент, ожидающий совместимый интерфейс
const NewComponent = ({ username }) => {
  console.log(`Hello, ${username}!`);
};

// Адаптер
const Adapter = ({ name }) => {
  const username = name.toUpperCase();
  return <NewComponent username={username} />;
};

// Использование адаптера
const App = () => {
  return <Adapter name="John" />;
};

ReactDOM.render(<App />, document.getElementById("root"));
```

В данном примере у нас есть функциональный компонент `LegacyComponent`, который принимает пропс `name` и выводит приветствие. У нас также есть компонент `NewComponent`, который ожидает пропс `username` и выводит приветствие. Их интерфейсы несовместимы. Мы создаем адаптер `Adapter`, который принимает пропс `name`, преобразует его в `username` и передает его в `NewComponent`. Таким образом, адаптер позволяет функциональному компоненту с несовместимым интерфейсом работать с компонентом, ожидающим совместимый интерфейс.

В обоих примерах паттерн адаптер позволяет объектам с несовместимыми интерфейсами работать вместе, обеспечивая совместимость между ними и возможность работы вместе без необходимости изменения их исходного кода.