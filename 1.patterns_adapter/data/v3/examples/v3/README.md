# Примеры использования

## Пример 1

В этом примере мы рассмотрим использование паттерна адаптер для адаптации двух компонентов с разными интерфейсами, чтобы они могли работать вместе.

```jsx
// Компонент, который нужно адаптировать
class LegacyComponent {
  doSomethingLegacy() {
    // Логика работы старого компонента
  }
}

// Целевой интерфейс, ожидаемый другими компонентами
class TargetInterface {
  doSomething() {
    // Логика работы целевого интерфейса
  }
}

// Адаптер
class Adapter extends TargetInterface {
  constructor(legacyComponent) {
    super();
    this.legacyComponent = legacyComponent;
  }

  doSomething() {
    this.legacyComponent.doSomethingLegacy();
  }
}

// Использование адаптера
const legacyComponent = new LegacyComponent();
const adapter = new Adapter(legacyComponent);

adapter.doSomething(); // Вызов метода целевого интерфейса, адаптированный к старому компоненту
```

## Пример 2

В этом примере мы рассмотрим использование паттерна адаптер для адаптации API запросов к разным сервисам.

```jsx
// Адаптер для работы с сервисом A
class ServiceAAdapter {
  constructor(serviceA) {
    this.serviceA = serviceA;
  }

  request() {
    return this.serviceA.makeRequest();
  }
}

// Адаптер для работы с сервисом B
class ServiceBAdapter {
  constructor(serviceB) {
    this.serviceB = serviceB;
  }

  request() {
    return this.serviceB.sendRequest();
  }
}

// Использование адаптеров
const serviceA = new ServiceA();
const serviceAAdapter = new ServiceAAdapter(serviceA);
serviceAAdapter.request(); // Вызов метода адаптированного интерфейса для сервиса A

const serviceB = new ServiceB();
const serviceBAdapter = new ServiceBAdapter(serviceB);
serviceBAdapter.request(); // Вызов метода адаптированного интерфейса для сервиса B
```

В этих примерах мы видим, как паттерн адаптер позволяет адаптировать объекты с разными интерфейсами для работы вместе. В первом примере адаптер преобразует вызовы методов целевого интерфейса к вызовам методов старого компонента. Во втором примере адаптеры преобразуют вызовы методов разных сервисов к вызовам методов адаптированного интерфейса.