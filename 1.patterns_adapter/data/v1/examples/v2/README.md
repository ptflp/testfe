# Примеры использования

## Пример 1

В этом примере мы рассмотрим использование паттерна адаптер для интеграции двух разных библиотек в React JS. Предположим, у нас есть библиотека `LibraryA` с интерфейсом:

```javascript
class LibraryA {
  methodA() {
    // Логика метода A
  }
}
```

И у нас также есть библиотека `LibraryB` с интерфейсом:

```javascript
class LibraryB {
  methodB() {
    // Логика метода B
  }
}
```

Мы хотим использовать оба этих класса в нашем React-компоненте. Для этого мы можем создать адаптер, который преобразует вызовы методов `LibraryA` и `LibraryB` в вызовы методов, ожидаемых React-компонентом.

```javascript
class Adapter {
  constructor() {
    this.libraryA = new LibraryA();
    this.libraryB = new LibraryB();
  }

  methodA() {
    this.libraryA.methodA();
  }

  methodB() {
    this.libraryB.methodB();
  }
}
```

Теперь мы можем использовать адаптер в нашем React-компоненте:

```javascript
class MyComponent extends React.Component {
  constructor() {
    super();
    this.adapter = new Adapter();
  }

  componentDidMount() {
    this.adapter.methodA();
    this.adapter.methodB();
  }

  render() {
    return <div>Пример использования паттерна адаптер в React JS</div>;
  }
}
```

В этом примере мы создали адаптер, который интегрирует `LibraryA` и `LibraryB` в наш React-компонент. Мы вызываем методы `methodA` и `methodB` через адаптер, что позволяет нам работать с обоими библиотеками без проблем.

## Пример 2

В этом примере мы рассмотрим использование паттерна адаптер для интеграции сторонней библиотеки `ExternalLibrary` с React JS. Предположим, у нас есть библиотека `ExternalLibrary` с интерфейсом:

```javascript
class ExternalLibrary {
  externalMethod() {
    // Логика внешнего метода
  }
}
```

Мы хотим использовать эту библиотеку в нашем React-компоненте. Для этого мы можем создать адаптер, который преобразует вызовы методов `ExternalLibrary` в вызовы методов, ожидаемых React-компонентом.

```javascript
class Adapter {
  constructor() {
    this.externalLibrary = new ExternalLibrary();
  }

  methodA() {
    this.externalLibrary.externalMethod();
  }
}
```

Теперь мы можем использовать адаптер в нашем React-компоненте:

```javascript
class MyComponent extends React.Component {
  constructor() {
    super();
    this.adapter = new Adapter();
  }

  componentDidMount() {
    this.adapter.methodA();
  }

  render() {
    return <div>Пример использования паттерна адаптер в React JS</div>;
  }
}
```

В этом примере мы создали адаптер, который интегрирует `ExternalLibrary` в наш React-компонент. Мы вызываем метод `methodA` через адаптер, что позволяет нам работать с внешней библиотекой без проблем.

В обоих примерах паттерн адаптер позволяет нам интегрировать объекты с несовместимыми интерфейсами в React JS и обеспечивает гибкость и расширяемость системы.