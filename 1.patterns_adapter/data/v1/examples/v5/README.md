# Примеры использования

## Пример 1

В этом примере мы рассмотрим использование паттерна адаптер для интеграции двух разных компонентов в React JS. У нас есть компонент `ComponentA`, который имеет свой собственный интерфейс, и компонент `ComponentB`, который имеет другой интерфейс. Мы хотим использовать `ComponentB` внутри `ComponentA`, но их интерфейсы несовместимы.

```jsx
// Компонент ComponentA
class ComponentA extends React.Component {
  render() {
    // ...
    return (
      <div>
        {/* ... */}
        <ComponentBAdapter />
        {/* ... */}
      </div>
    );
  }
}

// Компонент ComponentB
class ComponentB {
  // Интерфейс ComponentB
  methodB() {
    // ...
  }
}

// Адаптер для ComponentB
class ComponentBAdapter extends React.Component {
  componentDidMount() {
    const componentB = new ComponentB();
    componentB.methodB();
  }

  render() {
    return null;
  }
}
```

В этом примере мы создаем адаптер `ComponentBAdapter`, который реализует интерфейс, ожидаемый `ComponentA`. В методе `componentDidMount` адаптер создает экземпляр `ComponentB` и вызывает его метод `methodB`. Таким образом, мы успешно интегрируем `ComponentB` внутри `ComponentA`, используя паттерн адаптер.

## Пример 2

В этом примере мы рассмотрим использование паттерна адаптер для интеграции сторонней библиотеки в React JS. У нас есть библиотека `ExternalLibrary`, которая имеет свой собственный интерфейс, и мы хотим использовать ее функциональность внутри компонента React.

```jsx
import ExternalLibrary from 'external-library';

// Адаптер для ExternalLibrary
class ExternalLibraryAdapter {
  constructor() {
    this.externalLibrary = new ExternalLibrary();
  }

  // Методы адаптера, преобразующие вызовы методов ExternalLibrary
  methodA() {
    this.externalLibrary.someMethodA();
  }

  methodB() {
    this.externalLibrary.someMethodB();
  }

  // ...
}

// Компонент React, использующий ExternalLibrary через адаптер
class MyComponent extends React.Component {
  componentDidMount() {
    const externalLibraryAdapter = new ExternalLibraryAdapter();
    externalLibraryAdapter.methodA();
    externalLibraryAdapter.methodB();
  }

  render() {
    return <div>My Component</div>;
  }
}
```

В этом примере мы создаем адаптер `ExternalLibraryAdapter`, который оборачивает функциональность `ExternalLibrary` и предоставляет методы, совместимые с ожидаемым интерфейсом в React. В методе `componentDidMount` компонента `MyComponent` мы создаем экземпляр адаптера и вызываем его методы, которые в свою очередь вызывают соответствующие методы `ExternalLibrary`. Таким образом, мы успешно интегрируем стороннюю библиотеку в React JS с помощью паттерна адаптер.