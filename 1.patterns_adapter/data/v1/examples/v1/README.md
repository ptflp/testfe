# Примеры использования

## Пример 1

В этом примере мы рассмотрим использование паттерна адаптер для преобразования интерфейса одного класса в интерфейс другого класса.

```jsx
// Адаптируемый класс
class LegacyComponent {
  doSomethingLegacy() {
    console.log("Выполняется устаревшая логика");
  }
}

// Целевой интерфейс
class TargetInterface {
  doSomething() {
    console.log("Выполняется новая логика");
  }
}

// Адаптер
class Adapter extends TargetInterface {
  constructor() {
    super();
    this.legacyComponent = new LegacyComponent();
  }

  doSomething() {
    this.legacyComponent.doSomethingLegacy();
  }
}

// Использование адаптера
const adapter = new Adapter();
adapter.doSomething(); // Выполняется устаревшая логика
```

В этом примере у нас есть адаптируемый класс `LegacyComponent`, который содержит устаревшую логику. Мы хотим использовать новый интерфейс `TargetInterface`, но его метод `doSomething()` несовместим с методом `doSomethingLegacy()` адаптируемого класса. Для решения этой проблемы мы создаем адаптер, который наследуется от `TargetInterface` и использует экземпляр адаптируемого класса для выполнения запросов. Теперь мы можем вызывать метод `doSomething()` адаптера, который в свою очередь вызывает метод `doSomethingLegacy()` адаптируемого класса.

## Пример 2

В этом примере мы рассмотрим использование паттерна адаптер в React JS для интеграции с другой библиотекой.

```jsx
// Адаптируемая библиотека
class LegacyLibrary {
  doSomethingLegacy() {
    console.log("Выполняется устаревшая логика");
  }
}

// Адаптер
class ReactAdapter extends React.Component {
  constructor(props) {
    super(props);
    this.legacyLibrary = new LegacyLibrary();
  }

  componentDidMount() {
    this.legacyLibrary.doSomethingLegacy();
  }

  render() {
    return <div>Адаптер для интеграции с устаревшей библиотекой</div>;
  }
}

// Использование адаптера в React-компоненте
function App() {
  return (
    <div>
      <h1>Пример использования адаптера в React JS</h1>
      <ReactAdapter />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById("root"));
```

В этом примере у нас есть адаптируемая библиотека `LegacyLibrary`, которая содержит устаревшую логику. Мы хотим использовать эту библиотеку в React-компоненте, но ее интерфейс несовместим с интерфейсом React. Для интеграции мы создаем адаптер `ReactAdapter`, который наследуется от `React.Component` и использует экземпляр адаптируемой библиотеки для выполнения запросов. В методе `componentDidMount()` адаптер вызывает метод `doSomethingLegacy()` адаптируемой библиотеки. Затем мы используем адаптер в React-компоненте `App`, который рендерит адаптер и выводит сообщение о его использовании.

Это два примера использования паттерна адаптер в JavaScript с использованием React JS. Первый пример демонстрирует преобразование интерфейса одного класса в интерфейс другого класса, а второй пример показывает интеграцию с устаревшей библиотекой в React-компоненте.