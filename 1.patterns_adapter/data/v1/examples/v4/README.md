# Примеры использования

## Пример 1

В этом примере мы рассмотрим использование паттерна адаптер в React JS для интеграции с другой библиотекой, имеющей разный интерфейс.

```jsx
// Адаптируемый класс с несовместимым интерфейсом
class LegacyLibrary {
  doSomethingLegacy() {
    // Логика работы с устаревшей библиотекой
  }
}

// Адаптер
class Adapter {
  constructor() {
    this.legacyLibrary = new LegacyLibrary();
  }

  // Методы, соответствующие целевому интерфейсу
  doSomething() {
    this.legacyLibrary.doSomethingLegacy();
  }
}

// Компонент React, использующий адаптер
class MyComponent extends React.Component {
  constructor(props) {
    super(props);
    this.adapter = new Adapter();
  }

  componentDidMount() {
    this.adapter.doSomething();
  }

  render() {
    return <div>Пример использования паттерна адаптер в React JS</div>;
  }
}
```

## Пример 2

В этом примере мы рассмотрим использование паттерна адаптер для интеграции с внешним API, имеющим отличный от React JS интерфейс.

```jsx
// Адаптируемый класс с несовместимым интерфейсом
class ExternalAPI {
  request(data) {
    // Логика отправки запроса к внешнему API
  }
}

// Адаптер
class Adapter {
  constructor() {
    this.externalAPI = new ExternalAPI();
  }

  // Методы, соответствующие целевому интерфейсу
  makeRequest(data) {
    this.externalAPI.request(data);
  }
}

// Компонент React, использующий адаптер
class MyComponent extends React.Component {
  constructor(props) {
    super(props);
    this.adapter = new Adapter();
  }

  componentDidMount() {
    this.adapter.makeRequest({ key: 'value' });
  }

  render() {
    return <div>Пример использования паттерна адаптер в React JS</div>;
  }
}
```

В этих примерах мы создаем адаптеры, которые позволяют интегрировать объекты с несовместимыми интерфейсами в компоненты React JS. Адаптеры преобразуют вызовы методов целевого интерфейса в вызовы методов адаптируемых классов, обеспечивая совместимость и интеграцию между различными компонентами и библиотеками.