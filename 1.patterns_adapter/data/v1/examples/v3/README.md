# Примеры использования

## Пример 1

В этом примере мы рассмотрим использование паттерна адаптер для интеграции двух разных компонентов в React JS. У нас есть компонент `Button` из библиотеки React и компонент `LegacyButton` из старой библиотеки, которые имеют разные интерфейсы. Мы хотим использовать `LegacyButton` вместо `Button` в нашем приложении, но для этого нам нужно создать адаптер.

```jsx
// Компонент Button из библиотеки React
const Button = ({ onClick, children }) => {
  return <button onClick={onClick}>{children}</button>;
};

// Компонент LegacyButton из старой библиотеки
const LegacyButton = (text, action) => {
  // Логика и методы LegacyButton
};

// Адаптер для использования LegacyButton вместо Button
const LegacyButtonAdapter = ({ onClick, children }) => {
  const legacyButtonAction = () => {
    // Преобразование вызова onClick в вызов legacyButtonAction
    onClick();
  };

  return <LegacyButton action={legacyButtonAction}>{children}</LegacyButton>;
};

// Использование адаптера вместо Button
const App = () => {
  const handleClick = () => {
    console.log("Button clicked");
  };

  return (
    <div>
      <Button onClick={handleClick}>Click me</Button>
      <LegacyButtonAdapter onClick={handleClick}>Click me (Legacy)</LegacyButtonAdapter>
    </div>
  );
};
```

В этом примере мы создали адаптер `LegacyButtonAdapter`, который преобразует вызовы метода `onClick` компонента `Button` в вызовы метода `legacyButtonAction` компонента `LegacyButton`. Теперь мы можем использовать `LegacyButtonAdapter` вместо `Button` и обеспечить совместимость между двумя компонентами.

## Пример 2

В этом примере мы рассмотрим использование паттерна адаптер для интеграции с внешним API в React JS. У нас есть компонент `UserList`, который отображает список пользователей, полученных из внешнего API. Однако, API возвращает данные в другом формате, чем ожидает `UserList`. Мы создадим адаптер, который преобразует данные из API в нужный формат.

```jsx
// Компонент UserList
const UserList = ({ users }) => {
  return (
    <ul>
      {users.map((user) => (
        <li key={user.id}>{user.name}</li>
      ))}
    </ul>
  );
};

// Адаптер для интеграции с внешним API
const UserListAdapter = ({ apiData }) => {
  const adaptData = () => {
    // Преобразование данных из API в нужный формат
    return apiData.map((data) => ({
      id: data.userId,
      name: data.userName,
    }));
  };

  const adaptedData = adaptData();

  return <UserList users={adaptedData} />;
};

// Использование адаптера для отображения списка пользователей
const App = () => {
  const apiData = [
    { userId: 1, userName: "John" },
    { userId: 2, userName: "Jane" },
    { userId: 3, userName: "Bob" },
  ];

  return <UserListAdapter apiData={apiData} />;
};
```

В этом примере мы создали адаптер `UserListAdapter`, который преобразует данные из внешнего API в нужный формат для компонента `UserList`. Теперь мы можем использовать `UserListAdapter` для отображения списка пользователей, полученных из внешнего API, и обеспечить совместимость между форматами данных.

Это лишь два примера использования паттерна адаптер в React JS. Паттерн адаптер может быть применен в различных ситуациях, где требуется преобразование интерфейсов объектов для обеспечения их совместимости.