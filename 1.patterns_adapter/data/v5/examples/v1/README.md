# Примеры использования

## Пример 1

В этом примере мы рассмотрим использование паттерна адаптер для преобразования интерфейса одного класса в интерфейс другого класса.

```jsx
// Интерфейс целевого класса
class TargetClass {
  request() {
    console.log("Вызван метод request у целевого класса");
  }
}

// Интерфейс адаптируемого класса
class AdapteeClass {
  specificRequest() {
    console.log("Вызван метод specificRequest у адаптируемого класса");
  }
}

// Адаптер
class Adapter extends TargetClass {
  constructor(adaptee) {
    super();
    this.adaptee = adaptee;
  }

  request() {
    this.adaptee.specificRequest();
  }
}

// Использование адаптера
const adaptee = new AdapteeClass();
const adapter = new Adapter(adaptee);
adapter.request();
```

В этом примере у нас есть целевой класс `TargetClass`, который имеет метод `request`. Также у нас есть адаптируемый класс `AdapteeClass`, который имеет метод `specificRequest`. Мы создаем адаптер `Adapter`, который наследуется от `TargetClass` и преобразует вызов метода `request` в вызов метода `specificRequest` у адаптируемого класса. В результате, при вызове метода `request` у адаптера, будет вызван метод `specificRequest` у адаптируемого класса.

## Пример 2

В этом примере мы рассмотрим использование паттерна адаптер для расширения функциональности существующего класса.

```jsx
// Существующий класс
class ExistingClass {
  operation() {
    console.log("Вызван метод operation у существующего класса");
  }
}

// Адаптер
class Adapter {
  constructor(existingClass) {
    this.existingClass = existingClass;
  }

  extendedOperation() {
    this.existingClass.operation();
    console.log("Вызван расширенный метод extendedOperation у адаптера");
  }
}

// Использование адаптера
const existingClass = new ExistingClass();
const adapter = new Adapter(existingClass);
adapter.extendedOperation();
```

В этом примере у нас есть существующий класс `ExistingClass`, который имеет метод `operation`. Мы создаем адаптер `Adapter`, который принимает экземпляр существующего класса и добавляет новый метод `extendedOperation`. При вызове метода `extendedOperation` у адаптера, сначала будет вызван метод `operation` у существующего класса, а затем будет выполнено дополнительное действие в расширенном методе `extendedOperation` у адаптера.

...

Здесь должны быть представлены другие примеры использования паттерна адаптер.