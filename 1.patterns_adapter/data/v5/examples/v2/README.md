# Примеры использования

## Пример 1

В этом примере мы рассмотрим использование паттерна адаптер для преобразования интерфейса одного класса в интерфейс другого класса.

```jsx
// Интерфейс целевого класса
class TargetClass {
  request() {
    console.log("Вызван метод request у целевого класса");
  }
}

// Интерфейс адаптируемого класса
class AdapteeClass {
  specificRequest() {
    console.log("Вызван метод specificRequest у адаптируемого класса");
  }
}

// Адаптер
class Adapter extends TargetClass {
  constructor(adaptee) {
    super();
    this.adaptee = adaptee;
  }

  request() {
    this.adaptee.specificRequest();
  }
}

// Использование адаптера
const adaptee = new AdapteeClass();
const adapter = new Adapter(adaptee);
adapter.request();
```

В данном примере у нас есть целевой класс `TargetClass` с методом `request`, и адаптируемый класс `AdapteeClass` с методом `specificRequest`. Мы создаем адаптер `Adapter`, который наследуется от целевого класса и преобразует вызов метода `request` в вызов метода `specificRequest` у адаптируемого класса. Затем мы создаем экземпляр адаптируемого класса и адаптера, и вызываем метод `request` у адаптера. В результате будет вызван метод `specificRequest` у адаптируемого класса.

## Пример 2

В этом примере мы рассмотрим использование паттерна адаптер для адаптации сторонней библиотеки к интерфейсу React компонента.

```jsx
import ThirdPartyLibrary from 'third-party-library';

// Адаптер для сторонней библиотеки
class ThirdPartyAdapter extends React.Component {
  constructor(props) {
    super(props);
    this.thirdPartyLibrary = new ThirdPartyLibrary();
  }

  componentDidMount() {
    this.thirdPartyLibrary.init();
  }

  componentWillUnmount() {
    this.thirdPartyLibrary.cleanup();
  }

  render() {
    return (
      <div>
        {/* Взаимодействие с адаптированной сторонней библиотекой */}
      </div>
    );
  }
}

// Использование адаптера
function App() {
  return (
    <div>
      <h1>Мое React приложение</h1>
      <ThirdPartyAdapter />
    </div>
  );
}
```

В данном примере у нас есть сторонняя библиотека `ThirdPartyLibrary`, которую мы хотим использовать в нашем React приложении. Мы создаем адаптер `ThirdPartyAdapter`, который является React компонентом и адаптирует вызовы методов сторонней библиотеки к жизненным циклам React компонента. В методе `componentDidMount` мы инициализируем стороннюю библиотеку, а в методе `componentWillUnmount` выполняем необходимые действия по очистке. Затем мы используем адаптер в нашем React приложении, включая его в компонент `App`.