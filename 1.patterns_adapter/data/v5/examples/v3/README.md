# Примеры использования

## Пример 1

В этом примере мы рассмотрим использование паттерна адаптер для преобразования интерфейса одного класса в интерфейс другого класса.

```jsx
// Интерфейс целевого класса
class TargetClass {
  request() {
    console.log("Вызван метод request у целевого класса");
  }
}

// Интерфейс адаптируемого класса
class AdapteeClass {
  specificRequest() {
    console.log("Вызван метод specificRequest у адаптируемого класса");
  }
}

// Адаптер
class Adapter extends TargetClass {
  constructor(adaptee) {
    super();
    this.adaptee = adaptee;
  }

  request() {
    this.adaptee.specificRequest();
  }
}

// Использование адаптера
const adaptee = new AdapteeClass();
const adapter = new Adapter(adaptee);
adapter.request();
```

В данном примере у нас есть целевой класс `TargetClass`, который имеет метод `request`. Также у нас есть адаптируемый класс `AdapteeClass`, у которого есть метод `specificRequest`. Мы создаем адаптер `Adapter`, который наследуется от `TargetClass` и преобразует вызов метода `request` в вызов метода `specificRequest` у адаптируемого класса. В результате, при вызове метода `request` у адаптера, будет вызван метод `specificRequest` у адаптируемого класса.

## Пример 2

В этом примере мы рассмотрим использование паттерна адаптер для работы с разными классами, имеющими разные интерфейсы, но совместимые по функциональности.

```jsx
// Интерфейс целевого класса
class TargetClass {
  request() {
    console.log("Вызван метод request у целевого класса");
  }
}

// Адаптируемый класс 1
class AdapteeClass1 {
  specificRequest1() {
    console.log("Вызван метод specificRequest1 у адаптируемого класса 1");
  }
}

// Адаптируемый класс 2
class AdapteeClass2 {
  specificRequest2() {
    console.log("Вызван метод specificRequest2 у адаптируемого класса 2");
  }
}

// Адаптер для адаптируемого класса 1
class Adapter1 extends TargetClass {
  constructor(adaptee) {
    super();
    this.adaptee = adaptee;
  }

  request() {
    this.adaptee.specificRequest1();
  }
}

// Адаптер для адаптируемого класса 2
class Adapter2 extends TargetClass {
  constructor(adaptee) {
    super();
    this.adaptee = adaptee;
  }

  request() {
    this.adaptee.specificRequest2();
  }
}

// Использование адаптеров
const adaptee1 = new AdapteeClass1();
const adapter1 = new Adapter1(adaptee1);
adapter1.request();

const adaptee2 = new AdapteeClass2();
const adapter2 = new Adapter2(adaptee2);
adapter2.request();
```

В данном примере у нас есть целевой класс `TargetClass`, а также два адаптируемых класса `AdapteeClass1` и `AdapteeClass2`. Мы создаем два адаптера `Adapter1` и `Adapter2`, которые преобразуют вызовы метода `request` в вызовы соответствующих методов у адаптируемых классов. При использовании адаптеров, мы можем вызывать метод `request` у целевого класса, но на самом деле будут вызываться методы `specificRequest1` и `specificRequest2` у соответствующих адаптируемых классов.