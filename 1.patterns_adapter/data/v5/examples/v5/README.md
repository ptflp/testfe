# Примеры использования

## Пример 1

В этом примере мы рассмотрим использование паттерна адаптер для преобразования интерфейса одного класса в интерфейс другого класса.

```jsx
// Интерфейс целевого класса
class TargetClass {
  request() {
    console.log("Вызван метод request у целевого класса");
  }
}

// Интерфейс адаптируемого класса
class AdapteeClass {
  specificRequest() {
    console.log("Вызван метод specificRequest у адаптируемого класса");
  }
}

// Адаптер
class Adapter extends TargetClass {
  constructor(adaptee) {
    super();
    this.adaptee = adaptee;
  }

  request() {
    this.adaptee.specificRequest();
  }
}

// Использование адаптера
const adaptee = new AdapteeClass();
const adapter = new Adapter(adaptee);
adapter.request();
```

В данном примере у нас есть целевой класс `TargetClass`, который имеет метод `request`. Также у нас есть адаптируемый класс `AdapteeClass`, у которого есть метод `specificRequest`. Мы создаем адаптер `Adapter`, который наследуется от целевого класса и принимает адаптируемый класс в конструкторе. Метод `request` адаптера вызывает метод `specificRequest` адаптируемого класса. Таким образом, мы можем использовать адаптер для вызова метода `specificRequest` через интерфейс `request` целевого класса.

## Пример 2

В этом примере мы рассмотрим использование паттерна адаптер для адаптации сторонней библиотеки к интерфейсу React компонента.

```jsx
import React from 'react';
import ThirdPartyLibrary from 'third-party-library';

// Адаптер для сторонней библиотеки
class ThirdPartyAdapter extends React.Component {
  constructor(props) {
    super(props);
    this.thirdPartyLibrary = new ThirdPartyLibrary();
  }

  componentDidMount() {
    this.thirdPartyLibrary.init();
  }

  render() {
    return (
      <div>
        {/* Выводим результат работы сторонней библиотеки */}
        {this.thirdPartyLibrary.getResult()}
      </div>
    );
  }
}

// Использование адаптера
function App() {
  return (
    <div>
      <h1>Мое React приложение</h1>
      <ThirdPartyAdapter />
    </div>
  );
}

export default App;
```

В данном примере у нас есть сторонняя библиотека, которую мы хотим использовать в нашем React приложении. Мы создаем адаптер `ThirdPartyAdapter`, который является React компонентом. В конструкторе адаптера мы инициализируем стороннюю библиотеку и в методе `componentDidMount` вызываем метод `init`. В методе `render` адаптера мы выводим результат работы сторонней библиотеки. Затем мы используем адаптер в нашем React приложении, включая его в компонент `App`. Таким образом, мы адаптируем стороннюю библиотеку к интерфейсу React компонента и можем использовать ее в нашем приложении.

...

В приведенных примерах показано, как использовать паттерн адаптер для преобразования интерфейсов разных классов и библиотек. Паттерн адаптер позволяет объектам с несовместимыми интерфейсами работать вместе, обеспечивая гибкость и повторное использование кода.