# Примеры использования

## Пример 1

В данном примере мы рассмотрим использование паттерна адаптер в React JS для преобразования интерфейса компонента `OldComponent` в интерфейс компонента `NewComponent`.

```jsx
// Адаптируемый компонент
class OldComponent {
  render() {
    return <div>Старый компонент</div>;
  }
}

// Адаптер
class Adapter extends React.Component {
  render() {
    const oldComponent = new OldComponent();
    return oldComponent.render();
  }
}

// Клиентский код
function App() {
  return (
    <div>
      <h1>Пример использования паттерна адаптер в React JS</h1>
      <Adapter />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```

В данном примере мы создаем адаптер `Adapter`, который оборачивает компонент `OldComponent` и преобразует его интерфейс в интерфейс, ожидаемый клиентским кодом. Таким образом, мы можем использовать адаптированный компонент `Adapter` вместо ожидаемого компонента `NewComponent`, не изменяя код клиента.

## Пример 2

В данном примере мы рассмотрим использование паттерна адаптер в React JS для преобразования интерфейса сторонней библиотеки `ThirdPartyLibrary` в интерфейс компонента `MyComponent`.

```jsx
// Сторонняя библиотека с несовместимым интерфейсом
class ThirdPartyLibrary {
  doSomething() {
    console.log('Выполнение действия в сторонней библиотеке');
  }
}

// Адаптер
class Adapter extends React.Component {
  componentDidMount() {
    const thirdPartyLibrary = new ThirdPartyLibrary();
    thirdPartyLibrary.doSomething();
  }

  render() {
    return <div>Мой компонент</div>;
  }
}

// Клиентский код
function App() {
  return (
    <div>
      <h1>Пример использования паттерна адаптер в React JS</h1>
      <Adapter />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```

В данном примере мы создаем адаптер `Adapter`, который оборачивает стороннюю библиотеку `ThirdPartyLibrary` и преобразует вызовы методов ее интерфейса в вызовы соответствующих методов компонента `MyComponent`. Таким образом, мы можем использовать адаптированный компонент `Adapter` вместо ожидаемого компонента `MyComponent`, не изменяя код клиента.

...

В данном ответе представлены два примера использования паттерна адаптер в React JS. Первый пример демонстрирует адаптацию интерфейса компонента, а второй пример показывает адаптацию интерфейса сторонней библиотеки.