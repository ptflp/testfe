# Примеры использования

## Пример 1

В этом примере мы рассмотрим использование паттерна адаптер в React JS для преобразования интерфейса одного компонента в интерфейс другого компонента.

```jsx
// Целевой компонент
class TargetComponent extends React.Component {
  render() {
    return <div>Целевой компонент</div>;
  }
}

// Адаптируемый компонент
class AdapteeComponent {
  renderAdaptee() {
    return <div>Адаптируемый компонент</div>;
  }
}

// Адаптер
class AdapterComponent extends React.Component {
  render() {
    const adapteeComponent = new AdapteeComponent();
    return adapteeComponent.renderAdaptee();
  }
}

// Использование адаптера
function App() {
  return (
    <div>
      <TargetComponent />
      <AdapterComponent />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```

В этом примере у нас есть целевой компонент `TargetComponent`, который ожидает определенный интерфейс. У нас также есть адаптируемый компонент `AdapteeComponent`, который имеет несовместимый интерфейс с целевым компонентом. Мы создаем адаптер `AdapterComponent`, который оборачивает адаптируемый компонент и преобразует вызовы методов целевого интерфейса в вызовы методов адаптируемого компонента. В итоге, мы можем использовать адаптированный компонент вместо ожидаемого компонента без необходимости изменять код клиента.

## Пример 2

В этом примере мы рассмотрим более сложное использование паттерна адаптер в React JS для преобразования данных из одного формата в другой.

```jsx
// Целевой компонент
class TargetComponent extends React.Component {
  render() {
    const { data } = this.props;
    return (
      <div>
        <h1>{data.title}</h1>
        <p>{data.description}</p>
      </div>
    );
  }
}

// Адаптируемый компонент
class AdapteeComponent {
  renderAdaptee() {
    const adapteeData = {
      name: 'Example',
      content: 'This is an example',
    };
    return adapteeData;
  }
}

// Адаптер
class AdapterComponent extends React.Component {
  render() {
    const adapteeComponent = new AdapteeComponent();
    const adapteeData = adapteeComponent.renderAdaptee();
    const targetData = {
      title: adapteeData.name,
      description: adapteeData.content,
    };
    return <TargetComponent data={targetData} />;
  }
}

// Использование адаптера
function App() {
  return (
    <div>
      <AdapterComponent />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```

В этом примере у нас есть целевой компонент `TargetComponent`, который ожидает данные в определенном формате. У нас также есть адаптируемый компонент `AdapteeComponent`, который возвращает данные в другом формате. Мы создаем адаптер `AdapterComponent`, который вызывает метод адаптируемого компонента, преобразует данные из одного формата в другой и передает их в целевой компонент. В итоге, мы можем использовать адаптированные данные в целевом компоненте без необходимости изменять его код.

Это лишь два примера использования паттерна адаптер в React JS. Паттерн адаптер может быть применен в различных ситуациях, где требуется преобразование интерфейсов или данных для обеспечения совместимости между компонентами.