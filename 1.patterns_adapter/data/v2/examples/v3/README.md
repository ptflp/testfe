# Примеры использования

## Пример 1

В этом примере мы рассмотрим использование паттерна адаптер в React JS для преобразования интерфейса одного компонента в интерфейс другого компонента.

```jsx
// Целевой компонент
class TargetComponent extends React.Component {
  render() {
    return <div>Целевой компонент</div>;
  }
}

// Адаптируемый компонент
class AdapteeComponent {
  renderAdaptee() {
    return <div>Адаптируемый компонент</div>;
  }
}

// Адаптер
class AdapterComponent extends React.Component {
  render() {
    const adaptee = new AdapteeComponent();
    return adaptee.renderAdaptee();
  }
}

// Использование адаптера
function App() {
  return (
    <div>
      <TargetComponent />
      <AdapterComponent />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```

В этом примере у нас есть целевой компонент `TargetComponent`, который ожидает определенный интерфейс. У нас также есть адаптируемый компонент `AdapteeComponent`, который имеет несовместимый интерфейс с целевым компонентом. Мы создаем адаптер `AdapterComponent`, который оборачивает адаптируемый компонент и преобразует вызовы методов целевого интерфейса в вызовы методов адаптируемого компонента. В итоге мы можем использовать адаптированный компонент вместо ожидаемого компонента без изменения кода клиента.

## Пример 2

В этом примере мы рассмотрим более сложное использование паттерна адаптер для интеграции с внешним API.

```jsx
// Целевой компонент
class TargetComponent extends React.Component {
  componentDidMount() {
    // Вызов метода целевого интерфейса
    this.fetchData();
  }

  fetchData() {
    // Запрос к внешнему API через адаптер
    const adapter = new APIAdapter();
    adapter.fetchData()
      .then((data) => {
        // Обработка полученных данных
        console.log(data);
      })
      .catch((error) => {
        // Обработка ошибок
        console.error(error);
      });
  }

  render() {
    return <div>Целевой компонент</div>;
  }
}

// Адаптер для внешнего API
class APIAdapter {
  fetchData() {
    // Вызов методов адаптируемого API
    return ExternalAPI.getData();
  }
}

// Адаптируемый внешний API
class ExternalAPI {
  static getData() {
    // Запрос к внешнему API
    return fetch('https://api.example.com/data')
      .then((response) => response.json());
  }
}

// Использование адаптера
function App() {
  return (
    <div>
      <TargetComponent />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```

В этом примере у нас есть целевой компонент `TargetComponent`, который при монтировании вызывает метод `fetchData()`. Внутри этого метода мы используем адаптер `APIAdapter`, который оборачивает вызов методов адаптируемого внешнего API `ExternalAPI`. Адаптер преобразует вызовы методов целевого интерфейса в вызовы методов адаптируемого API. Таким образом, мы можем интегрировать внешнее API с несовместимым интерфейсом в наш целевой компонент без изменения его кода.

Это лишь два примера использования паттерна адаптер в React JS. Паттерн адаптер может быть применен в различных ситуациях, где требуется совместимость объектов с несовместимыми интерфейсами.