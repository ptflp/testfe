# Примеры использования

## Пример 1

В этом примере мы рассмотрим использование паттерна адаптер для интеграции двух компонентов с разными интерфейсами в React JS.

```jsx
// Адаптируемый компонент
class LegacyComponent {
  doSomethingLegacy() {
    // Реализация функциональности для старого компонента
  }
}

// Целевой компонент
class NewComponent extends React.Component {
  doSomethingNew() {
    // Реализация функциональности для нового компонента
  }

  render() {
    return <div>New Component</div>;
  }
}

// Адаптерный компонент
class AdapterComponent extends React.Component {
  constructor(props) {
    super(props);
    this.legacyComponent = new LegacyComponent();
  }

  componentDidMount() {
    this.legacyComponent.doSomethingLegacy();
  }

  render() {
    return <NewComponent />;
  }
}

// Использование адаптерного компонента в приложении
function App() {
  return (
    <div>
      <h1>My App</h1>
      <AdapterComponent />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```

В этом примере у нас есть два компонента: `LegacyComponent` и `NewComponent`. `LegacyComponent` является старым компонентом с несовместимым интерфейсом, а `NewComponent` является новым компонентом с ожидаемым интерфейсом. Мы создаем адаптерный компонент `AdapterComponent`, который использует `LegacyComponent` для выполнения функциональности и отображает `NewComponent` внутри себя. Таким образом, мы можем интегрировать `LegacyComponent` с несовместимым интерфейсом в наше приложение на React JS.

## Пример 2

В этом примере мы рассмотрим использование паттерна адаптер для интеграции внешней библиотеки с разным интерфейсом в React JS.

```jsx
// Адаптируемая библиотека
class ExternalLibrary {
  performAction() {
    // Реализация функциональности внешней библиотеки
  }
}

// Целевой компонент
class MyComponent extends React.Component {
  doSomething() {
    // Реализация функциональности для компонента
  }

  render() {
    return <div>My Component</div>;
  }
}

// Адаптерный компонент
class AdapterComponent extends React.Component {
  constructor(props) {
    super(props);
    this.externalLibrary = new ExternalLibrary();
  }

  componentDidMount() {
    this.externalLibrary.performAction();
  }

  render() {
    return <MyComponent />;
  }
}

// Использование адаптерного компонента в приложении
function App() {
  return (
    <div>
      <h1>My App</h1>
      <AdapterComponent />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```

В этом примере у нас есть внешняя библиотека `ExternalLibrary`, которая имеет свой собственный интерфейс. Мы создаем адаптерный компонент `AdapterComponent`, который использует `ExternalLibrary` для выполнения функциональности и отображает `MyComponent` внутри себя. Таким образом, мы можем интегрировать внешнюю библиотеку с разным интерфейсом в наше приложение на React JS.

Это лишь два примера использования паттерна адаптер в React JS. В зависимости от конкретных требований и ситуаций, паттерн адаптер может быть применен для интеграции различных компонентов или библиотек с несовместимыми интерфейсами.