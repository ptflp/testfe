# Примеры использования

## Пример 1

В этом примере мы рассмотрим использование паттерна адаптер для интеграции двух компонентов с разными интерфейсами в React JS.

```jsx
// Адаптируемый компонент
class LegacyComponent {
  renderLegacyData() {
    // Рендеринг данных в старом формате
  }
}

// Целевой компонент
class NewComponent extends React.Component {
  render() {
    // Рендеринг данных в новом формате
  }
}

// Адаптерный компонент
class AdapterComponent extends React.Component {
  render() {
    const legacyComponent = new LegacyComponent();
    const legacyData = legacyComponent.renderLegacyData();

    // Преобразование данных в новый формат
    const newData = transformLegacyData(legacyData);

    // Передача данных в целевой компонент
    return <NewComponent data={newData} />;
  }
}

// Использование адаптерного компонента в React JS
function App() {
  return (
    <div>
      <h1>Пример использования паттерна адаптер</h1>
      <AdapterComponent />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```

В этом примере у нас есть адаптируемый компонент `LegacyComponent`, который рендерит данные в старом формате. У нас также есть целевой компонент `NewComponent`, который ожидает данные в новом формате. Мы создаем адаптерный компонент `AdapterComponent`, который использует `LegacyComponent` для получения данных в старом формате, преобразует их в новый формат и передает их в `NewComponent`. Таким образом, мы успешно интегрируем компоненты с разными интерфейсами с помощью паттерна адаптер.

## Пример 2

В этом примере мы рассмотрим использование паттерна адаптер для интеграции внешней библиотеки с React JS.

```jsx
// Адаптируемая библиотека
class ExternalLibrary {
  fetchData(callback) {
    // Получение данных из внешней библиотеки
    const data = fetchExternalData();

    // Вызов callback функции с полученными данными
    callback(data);
  }
}

// Целевой компонент
class MyComponent extends React.Component {
  componentDidMount() {
    const externalLibrary = new ExternalLibrary();

    // Использование адаптера для получения данных из внешней библиотеки
    externalLibrary.fetchData(this.handleData);
  }

  handleData(data) {
    // Обработка полученных данных
  }

  render() {
    // Рендеринг компонента
  }
}

// Использование целевого компонента в React JS
function App() {
  return (
    <div>
      <h1>Пример использования паттерна адаптер</h1>
      <MyComponent />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```

В этом примере у нас есть адаптируемая библиотека `ExternalLibrary`, которая предоставляет метод `fetchData` для получения данных из внешнего источника. У нас также есть целевой компонент `MyComponent`, который должен использовать данные из этой библиотеки. Мы создаем адаптер, который оборачивает вызов `fetchData` и передает полученные данные в `handleData` метод `MyComponent`. Таким образом, мы успешно интегрируем внешнюю библиотеку с React JS с помощью паттерна адаптер.