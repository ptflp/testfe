# Примеры использования

## Пример 1

В этом примере мы рассмотрим использование паттерна адаптер для интеграции двух компонентов с разными интерфейсами в React JS.

```jsx
// Адаптируемый компонент
class LegacyComponent {
  doSomethingLegacy() {
    // Логика работы с устаревшим интерфейсом
  }
}

// Целевой интерфейс
class NewComponent {
  doSomethingNew() {
    // Логика работы с новым интерфейсом
  }
}

// Адаптерный компонент
class AdapterComponent extends React.Component {
  constructor(props) {
    super(props);
    this.legacyComponent = new LegacyComponent();
  }

  componentDidMount() {
    this.legacyComponent.doSomethingLegacy();
  }

  render() {
    return <NewComponent />;
  }
}

// Использование адаптерного компонента
function App() {
  return (
    <div>
      <h1>Пример использования паттерна адаптер в React JS</h1>
      <AdapterComponent />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```

В этом примере мы создаем адаптерный компонент `AdapterComponent`, который интегрирует устаревший компонент `LegacyComponent` с новым компонентом `NewComponent`. Адаптерный компонент использует устаревший компонент для выполнения определенных действий и отображает новый компонент на странице. Таким образом, мы успешно интегрируем компоненты с разными интерфейсами в наш проект на React JS.

## Пример 2

В этом примере мы рассмотрим использование паттерна адаптер для интеграции внешней библиотеки с React JS.

```jsx
// Адаптируемая библиотека
const ExternalLibrary = {
  doSomethingExternal() {
    // Логика работы с внешней библиотекой
  }
};

// Целевой интерфейс
class NewComponent {
  doSomethingNew() {
    // Логика работы с новым интерфейсом
  }
}

// Адаптерный компонент
class AdapterComponent extends React.Component {
  componentDidMount() {
    ExternalLibrary.doSomethingExternal();
  }

  render() {
    return <NewComponent />;
  }
}

// Использование адаптерного компонента
function App() {
  return (
    <div>
      <h1>Пример использования паттерна адаптер в React JS</h1>
      <AdapterComponent />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```

В этом примере мы создаем адаптерный компонент `AdapterComponent`, который интегрирует внешнюю библиотеку `ExternalLibrary` с новым компонентом `NewComponent`. Адаптерный компонент использует внешнюю библиотеку для выполнения определенных действий и отображает новый компонент на странице. Таким образом, мы успешно интегрируем внешнюю библиотеку с разным интерфейсом в наш проект на React JS.

...