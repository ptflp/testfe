# Примеры использования

## Пример 1

В этом примере мы рассмотрим использование паттерна адаптер для интеграции двух компонентов с разными интерфейсами в React JS.

```jsx
// Адаптируемый компонент
class LegacyComponent {
  doSomethingLegacy() {
    // Логика адаптируемого компонента
  }
}

// Целевой компонент
class NewComponent extends React.Component {
  doSomethingNew() {
    // Логика целевого компонента
  }

  render() {
    return <div>New Component</div>;
  }
}

// Адаптерный компонент
class AdapterComponent extends React.Component {
  componentDidMount() {
    const legacyComponent = new LegacyComponent();
    legacyComponent.doSomethingLegacy(); // Вызов метода адаптируемого компонента

    const newComponent = new NewComponent();
    newComponent.doSomethingNew(); // Вызов метода целевого компонента
  }

  render() {
    return <div>Adapter Component</div>;
  }
}

// Использование адаптерного компонента в приложении React JS
function App() {
  return (
    <div>
      <AdapterComponent />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```

## Пример 2

В этом примере мы рассмотрим использование паттерна адаптер для интеграции сторонней библиотеки с React JS.

```jsx
// Адаптируемая библиотека
class ThirdPartyLibrary {
  doSomething() {
    // Логика адаптируемой библиотеки
  }
}

// Целевой компонент
class NewComponent extends React.Component {
  doSomethingNew() {
    // Логика целевого компонента
  }

  render() {
    return <div>New Component</div>;
  }
}

// Адаптерный компонент
class AdapterComponent extends React.Component {
  componentDidMount() {
    const thirdPartyLibrary = new ThirdPartyLibrary();
    thirdPartyLibrary.doSomething(); // Вызов метода адаптируемой библиотеки

    const newComponent = new NewComponent();
    newComponent.doSomethingNew(); // Вызов метода целевого компонента
  }

  render() {
    return <div>Adapter Component</div>;
  }
}

// Использование адаптерного компонента в приложении React JS
function App() {
  return (
    <div>
      <AdapterComponent />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```

В этих примерах мы использовали паттерн адаптер для интеграции компонентов и библиотек с разными интерфейсами в React JS. Адаптерный компонент служит промежуточным звеном, которое позволяет объектам с несовместимыми интерфейсами работать вместе.