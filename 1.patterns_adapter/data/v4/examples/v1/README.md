# Примеры использования

## Пример 1

В этом примере мы рассмотрим ситуацию, когда у нас есть два компонента с разными интерфейсами, и мы хотим интегрировать их вместе с помощью паттерна адаптер.

```jsx
// Адаптируемый компонент
class LegacyComponent {
  render() {
    return <div>Hello, Legacy Component!</div>;
  }
}

// Целевой компонент
class NewComponent extends React.Component {
  render() {
    return <div>Hello, New Component!</div>;
  }
}

// Адаптерный компонент
class AdapterComponent extends React.Component {
  render() {
    const legacyComponent = new LegacyComponent();
    return (
      <div>
        <NewComponent />
        {legacyComponent.render()}
      </div>
    );
  }
}

// Использование адаптерного компонента
function App() {
  return (
    <div>
      <h1>Пример использования паттерна адаптер в React JS</h1>
      <AdapterComponent />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```

В этом примере мы создаем адаптерный компонент `AdapterComponent`, который интегрирует два компонента с разными интерфейсами: `NewComponent` и `LegacyComponent`. Адаптерный компонент выводит оба компонента на странице, позволяя им работать вместе.

## Пример 2

В этом примере мы рассмотрим ситуацию, когда у нас есть библиотека с несовместимым интерфейсом, и мы хотим использовать ее в нашем проекте с помощью паттерна адаптер.

```jsx
// Адаптируемая библиотека
class LegacyLibrary {
  greet() {
    return 'Hello from Legacy Library!';
  }
}

// Адаптер для библиотеки
class LibraryAdapter {
  constructor() {
    this.legacyLibrary = new LegacyLibrary();
  }

  greet() {
    return this.legacyLibrary.greet();
  }
}

// Использование адаптера для библиотеки
function App() {
  const libraryAdapter = new LibraryAdapter();
  const greeting = libraryAdapter.greet();

  return (
    <div>
      <h1>Пример использования паттерна адаптер в React JS</h1>
      <p>{greeting}</p>
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById('root'));
```

В этом примере мы создаем адаптер `LibraryAdapter` для несовместимой библиотеки `LegacyLibrary`. Адаптер позволяет использовать функциональность библиотеки через совместимый интерфейс. В компоненте `App` мы создаем экземпляр адаптера и вызываем метод `greet()`, чтобы получить приветствие от библиотеки.

...

Здесь вы можете продолжить приводить другие примеры использования паттерна адаптер в React JS.